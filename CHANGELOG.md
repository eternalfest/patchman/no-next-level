# 0.12.1 (2024-11-20)

- **[Fix]** Mark package as public in the registry
- **[Internal]** Update to Yarn 4.5.1
- **[Internal]** Explicitly set package files

# 0.12.0 (2020-04-08)

- **[Feature]** First release.
