package no_next_level.proxies;

import hf.Hf;
import hf.mode.GameMode;
import merlin.INamespace;
import merlin.IProxy;
import merlin.IProxyFactory;
import merlin.ScriptValue;
import patchman.DebugConsole;
import patchman.Obfu;
import no_next_level.NoNextLevel in NoNextLevelMod;

/**
 * A proxy for a variable visible by the game.
 */
class NoNextLevel implements IProxyFactory {
  public var name(default, never): String = Obfu.raw("NO_NEXT_LEVEL");

  public function new() {}

  public function createProxy(hf: Hf, game: GameMode): IProxy {
    return new NoNextLevelProxy(game);
  }
}

class NoNextLevelProxy implements IProxy {
  private var game(default, null): GameMode;

  public function new(game: GameMode): Void {
    this.game = game;
  }

  public function sub(): Null<INamespace> {
    return null;
  }

  public function get(): Null<ScriptValue> {
    // Prefix with `$` because `getDynamicVar` removes the first character
    var gameValue: Dynamic = this.game.getDynamicVar(Obfu.raw("$") + NoNextLevelMod.SYMBOL.toString());
    // TODO: Check type
    return gameValue;
  }

  public function set(value: ScriptValue): Bool {
    var boolValue: Null<Bool> = value.asBool();
    if (boolValue == null) {
      DebugConsole.error(Obfu.raw("TypeError: Expected boolean for `NO_NEXT_LEVEL`"));
      return false;
    }
    if (boolValue) {
      this.game.fxMan.detachExit();
    } else {
      this.game.fxMan.attachExit();
    }
    // TODO: Check type
    this.game.setDynamicVar(NoNextLevelMod.SYMBOL.toString(), boolValue);
    return true;
  }
}
