package no_next_level;

import patchman.Symbol;
import hf.FxManager;
import hf.Hf;
import merlin.IProxyFactory;
import no_next_level.proxies.NoNextLevel in NoNextLevelProxyFactory;
import patchman.IPatch;
import patchman.Obfu;
import patchman.Ref;

/**
 * Adds the `NO_NEXT_LEVEL` proxy.
 *
 * Setting it to true disables the `FxManager.attachExit` (attach the level exit arrow)
 * for the current level.
 *
 * Example:
 * ```
 * NO_NEXT_LEVEL = true;
 * ```
 */
@:build(patchman.Build.di())
class NoNextLevel {
  public static var SYMBOL(default, never): Symbol = new Symbol();

  public static var PROXY_FACTORY(default, never): NoNextLevelProxyFactory = new NoNextLevelProxyFactory();

  public static var PATCH(default, null) = Ref.auto(FxManager.attachExit).wrap(function(hf: Hf, self: FxManager, next: FxManager -> Void): Void {
    var isDisabled: Dynamic = self.game.getDynamicVar(Obfu.raw("$") + SYMBOL.toString());
    if (!isDisabled) {
      next(self);
    }
  });

  @:diExport
  public var patch(default, null): IPatch;
  @:diExport
  public var proxyFactory(default, null): IProxyFactory;

  public function new(): Void {
    this.patch = PATCH;
    this.proxyFactory = PROXY_FACTORY;
  }
}
